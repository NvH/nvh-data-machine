NVH-DATA-MACHINE
Traitement de données depuis fichiers CSV, XLS ou XLSX
Partie backend du projet : JAVA SPRING + DB mySQL serveur Tomcat, projet 
MAVEN

v 1.0 SNAPSHOT (dérivée des sources de jan./fev. 2016)

* Reconnaissance des types de données par colonne depuis un fichier 
.csv, .xls ou .xlsx

* Enregistrement des données et des types de données trouvés dans une 
base de données HSQL embarquée (puis mySQL)

* Communication des résultats au format JSON via l'API Swagger : 
possibilité de "brancher" un frontend 

* Données enregistrées dans 3 tables : Deux à structure fixe et une 
créee à la volée en fonction des objets trouvés

v 1.1.0 SNAPSHOT (dérivée des sources de mars 2016)

* Nouveau modèle de données

* Enregistrement de tous les feuillets en base

* Version testable sur swagger : localhost:8080/swagger-ui.html
après lancement de nvhMachineApplication

v 1.1.1 SNAPSHOT

* Sauvegarde et lit plusieurs projets dans la base de données embarquée. Les 
projets sont identifiés comme suit : "REF_#Nom_Première_Feuille" pour le moment 
