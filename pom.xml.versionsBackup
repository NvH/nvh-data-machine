<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.nvh.nvh-machine-backend</groupId>
    <artifactId>nvh-machine-backend</artifactId>
    <version>1.0-SNAPSHOT</version>

    <packaging>pom</packaging>
    <name>nvh Machine Parent Project</name>

    <modules>
        <module>data-inspector</module>
        <module>webapp</module>
    </modules>

    <properties>
        <spring.boot.version>1.3.0.RELEASE</spring.boot.version>
        <spring.version>4.2.3.RELEASE</spring.version>
        <guava.version>19.0</guava.version>
        <spring.security.version>3.1.3.RELEASE</spring.security.version>
        <nvh-commons.version>3.0.0</nvh-commons.version>
        <logger.version>1.7.13</logger.version>
        <apache.commons.version>3.4</apache.commons.version>
        <junit.version>4.11</junit.version>
        <assertj.version>2.0.0</assertj.version>
        <commons.fileupload.version>1.2.2</commons.fileupload.version>
        <commons.io.version>2.4</commons.io.version>
        <swagger.version>2.2.2</swagger.version>
        <servlet.api.version>3.1.0</servlet.api.version>
        <mvn.war.version>2.2</mvn.war.version>
        <org.apache.poi.version>3.13</org.apache.poi.version>
        <spring-test.version>4.1.7.RELEASE</spring-test.version>
        <hsqldb.version>2.3.2</hsqldb.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring.boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!-- Spring JDBC
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-jdbc</artifactId>
                <version>${spring.version}</version>
            </dependency>
            -->
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-security</artifactId>
                <version>${spring.security.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>

        <!-- Logger -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${logger.version}</version>
        </dependency>

        <!-- commons librairies -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>${apache.commons.version}</version>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>${guava.version}</version>
        </dependency>

        <!-- HyperSQL DB -->
        <dependency>
            <groupId>org.hsqldb</groupId>
            <artifactId>hsqldb</artifactId>
            <version>${hsqldb.version}</version>
        </dependency>

        <!-- Tests utils -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>${assertj.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <!-- Package as an executable jar -->
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
