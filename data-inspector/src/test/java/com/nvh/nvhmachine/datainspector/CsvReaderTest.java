package com.nvh.nvhmachine.datainspector;

import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import com.nvh.nvhmachine.datainspector.reader.CsvToXls;
import com.nvh.nvhmachine.datainspector.reader.ExcelReader;
import org.apache.poi.ss.usermodel.Cell;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Map;

import static com.nvh.nvhmachine.datainspector.reader.ExcelReader.getTypeFromCellTypeConstants;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CsvReaderTest {


    private final static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(CsvReaderTest.class);
    private ExcelReader test;

    @Before
    public void setup() throws FileNotFoundException {

        URL resource = CsvReaderTest.class.getResource("/test.csv");
        test = new ExcelReader(CsvToXls.convert(new FileInputStream((resource.getFile()))));
    }

    @Test
    public void testConvert() {
        //valide le bon fonctionnement de CsvToXls
        assertNotNull(test.getWorkbook());
    }

    @Test
    public void testGetTypeFromCellTypeConstants() throws Exception {
        //"BOOLEAN" et "ERROR" sont reconnus en tant que "TEXT" sur les fichiers CSV

        Cell toTest;

        toTest = test.getWorkbook().getSheetAt(0).getRow(1).getCell(0);
        assertEquals(getTypeFromCellTypeConstants(toTest, toTest.getCellType()), "text");

        toTest = test.getWorkbook().getSheetAt(0).getRow(1).getCell(1);
        assertEquals(getTypeFromCellTypeConstants(toTest, toTest.getCellType()), "numeric");

        toTest = test.getWorkbook().getSheetAt(0).getRow(1).getCell(2);
        assertEquals(getTypeFromCellTypeConstants(toTest, toTest.getCellType()), "text");

        toTest = test.getWorkbook().getSheetAt(0).getRow(1).getCell(3);
        assertEquals(getTypeFromCellTypeConstants(toTest, toTest.getCellType()), "date");

        toTest = test.getWorkbook().getSheetAt(0).getRow(1).getCell(4);
        assertEquals(getTypeFromCellTypeConstants(toTest, toTest.getCellType()), "date");

        toTest = test.getWorkbook().getSheetAt(0).getRow(1).getCell(5);
        assertEquals(getTypeFromCellTypeConstants(toTest, toTest.getCellType()), "text");

        toTest = test.getWorkbook().getSheetAt(0).getRow(1).getCell(7);
        assertEquals(getTypeFromCellTypeConstants(toTest, toTest.getCellType()), "numeric");

    }

    @Test
    public void mainTest() {

        com.nvh.nvhmachine.datainspector.model.Workbook result = test.getInformation(4);
        //args : ligne de départ , nombre de lignes
        for (WorkbookSheet s : result.sheets) {

            LOGGER.info(String.valueOf(s.getColumns()));

            for (Map<String, String> c : s.getCells()) {
                LOGGER.info(String.valueOf(c));
            }
        }
    }
}

