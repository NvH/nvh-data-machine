package com.nvh.nvhmachine.datainspector;

import com.nvh.nvhmachine.datainspector.model.Workbook;
import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import com.nvh.nvhmachine.datainspector.reader.ExcelReader;
import org.apache.poi.ss.usermodel.Cell;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

import static com.nvh.nvhmachine.datainspector.reader.ExcelReader.getTypeFromCellTypeConstants;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ExcelReaderTest {

    private final static Logger LOGGER = LoggerFactory.getLogger(ExcelReaderTest.class);

    private ExcelReader test;

    @Before
    public void setup() throws IOException {

        URL resource = CsvReaderTest.class.getResource("/shoes.xlsx");
        test = new ExcelReader(new FileInputStream(resource.getFile()));
    }

    @Test
    public void mainTest() {

        Workbook result = null;

        try {
            result = test.getInformation(50);
            //par défaut : 50 lignes
            for (WorkbookSheet s : result.sheets) {

                LOGGER.info(String.valueOf(s.getColumns()));
                for (Map<String, String> c : s.getCells()) {
                    LOGGER.info(String.valueOf(c));
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {
            assertNotNull(result);
        }
    }

    @Test
    public void testGetTypeFromCellTypeConstants() throws Exception {

        Cell toTest;
        String[] expected = new String[]{
                "text",
                "numeric",
                "numeric",
                "numeric",
                "text",
                "numeric",
                "numeric",
                "numeric",
                "numeric",
                "text",
                "text",
                "text",
                "text",
                "text",
                "numeric",
                "text",
                "date"};

        for (int i = 0; i < expected.length; i++) {
            toTest = test.getWorkbook().getSheetAt(0).getRow(1).getCell(i);
            assertEquals(getTypeFromCellTypeConstants(toTest, toTest.getCellType()), expected[i]);
        }
    }
}
