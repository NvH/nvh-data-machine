package com.nvh.nvhmachine.datainspector.reader;


/**
 * Constantes de définition des 3 grands types de données numériques (Date, TimeStamp ou Nombre) sous forme
 * d'expression régulière
 */
enum ColumnTypes {
    TIMESTAMP(Constants.TIMESTAMP_REGEX),
    //de type : dd-mm-yyyy [AM?/PM?]hh:mm au format D-M-Y ou autre permutation
    DATE(Constants.DATE_REGEX),
    //de type : dd-mm-yyyy au format D-M-Y ou autre permutation
    NUMERIC(Constants.NUMERIC_REGEX);
    public final String regex;

    ColumnTypes(String regex) {
        this.regex = regex;
    }

    private static class Constants {
        static final String TIMESTAMP_REGEX = "^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\.\\d{3}(?: [AP]M)?(?: [+-]\\d{4})?$";
        static final String DATE_REGEX = "^[0-3]?[0-9]/[0-3]?[0-9]/(?:[0-9]{2})?[0-9]{2}$";
        static final String NUMERIC_REGEX = "^-?\\d+(,\\d+)*(\\.\\d+(e\\d+)?)?$";
    }
}
