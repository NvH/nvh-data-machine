package com.nvh.nvhmachine.datainspector.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Classe décrivant l'ensemble des 'columns' (titres et types de chaque colonne) et l'ensemble des cellules (contenu)
 * trouvés dans le fichier source
 * Chaque 'WorkbookSheet' est défini par un nom (standardisé), formé d'une référence au nom de l'utilisateur , du nom
 * du feuillet (v. 0 et v.1) puis de la date dans les versions ultérieures
 * et d'un index (N° de feuillet, pour les classeurs à plusieurs feuillets)
 */
public class WorkbookSheet {

    private String name;
    private int index;
    private List<Map<String, Object>> columns = new ArrayList<>();
    private List<Map<String, String>> cells = new ArrayList<>();

    public WorkbookSheet(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public WorkbookSheet() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Map<String, Object>> getColumns() {
        return columns;
    }

    public void setColumns(List<Map<String, Object>> columns) {
        this.columns = columns;
    }

    public List<Map<String, String>> getCells() {
        return cells;
    }

    public void setCells(List<Map<String, String>> cells) {
        this.cells = cells;
    }

    public String formatName() {
        return "REF_" + this.getName().replace(' ', '_');
        /*TODO : l'identifiant unique devra être standardisé pour reprendre ce qui a été défini dans la maquette
        v.0 et v.1 : l'identifiant se compose du nom de l'établissement et du nom du 1er feuillet importé
        REF : Nom de l'établissement (défini par l'utilisateur)
         */
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
