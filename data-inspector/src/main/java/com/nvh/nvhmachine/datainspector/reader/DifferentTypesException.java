package com.nvh.nvhmachine.datainspector.reader;

class DifferentTypesException extends RuntimeException {

    DifferentTypesException(String message) {
        super(message);
    }
}
