package com.nvh.nvhmachine.datainspector.reader;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Deprecated since 1.0
 * Before : Identifies data types of a sample of lines extracted from a CSV file.
 * The sample percent can be defined by user.
 *
 *
 * From 1.0 :
 * data-inspector uses CsvTXls and ExcelReader to scan CSV files
 * CSV and XLS files are therefore scanned entirely and data types are found after a exhaustive search.
 */
@Deprecated
public class CsvSampler {
    private final static Logger LOGGER = LoggerFactory.getLogger(CsvSampler.class);
    private List<String> types;

    public List<String> getStandardTypes(File file, int percent) {
        List<String> lines = new ArrayList<>();
        String line;

        try (BufferedReader in = new BufferedReader(new FileReader(file))) {
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return null;
        }
        types = getColumnsTypes(lines.get(0));
        validateColumnsTypes(percent, lines);
        return types;
    }

    private void validateColumnsTypes(int samplingRatePercent, List<String> buffer) {
        //Mélange pour obtention d'un échantillon aléatoire de lignes (x %)
        Collections.shuffle(buffer);
        int lineMax = getLineMax(samplingRatePercent, buffer);

        LOGGER.debug("[INFO] Nombre de lignes total :" + buffer.size());
        LOGGER.debug("[INFO] Taille de l'échantillon :" + lineMax);

        for (int a = 0; a < lineMax; a++) {
            List<String> typeToTest = getColumnsTypes(buffer.get(a));
            try {
                checkIfDeepEqual(types, typeToTest);
            } catch (DifferentTypesException dte) {
                LOGGER.error(dte.getMessage());
            }
        }
    }

    private int getLineMax(int percent, List<String> lines) {
        //s'assure que la taille de l'échantillon de dépasse pas celle de la liste
        return lines.size() < (percent * lines.size() / 100) ?
                lines.size() : (percent * lines.size() / 100);
    }

    public List<String> getColumnsTypes(String line) {

        if (StringUtils.isBlank(line)) {
            return null;
        }

        String[] cells = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
        List<String> types = new ArrayList<>();

        for (String cell : cells) {
            types.add(getColumnType(cell));
        }
        return types;
    }

    public String getColumnType(String cell) {

        if (cell.length() < 1) {
            return "UNKNOWN";
        }

        for (ColumnTypes type : ColumnTypes.values()) {
            if (cell.matches(type.name())) return type.toString();
        }

        if (NumberUtils.isNumber(cell)) {
            return "NUMERIC";
        }
        return "TEXT";
    }

    public void checkIfDeepEqual(List<String> arrayToBeCompared, List<String> arrayToTest) {

        for (int i = 0; i < arrayToBeCompared.size(); i++) {
            if (!arrayToBeCompared.get(i).equals(arrayToTest.get(i)) && !arrayToBeCompared.get(i).equals("UNKNOWN") && !arrayToTest.get(i).equals("UNKNOWN")) {

                throw new DifferentTypesException("Incompatinvhlité des données : les colonnes sont composées de types de données différents.");
            }
            if (arrayToBeCompared.get(i).equals("UNKNOWN") && !arrayToTest.get(i).equals("UNKNOWN")) {
                arrayToBeCompared.set(i, arrayToTest.get(i));
            }
        }
    }
}
