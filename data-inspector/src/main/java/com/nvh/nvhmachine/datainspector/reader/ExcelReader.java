package com.nvh.nvhmachine.datainspector.reader;

import com.nvh.nvhmachine.datainspector.model.Workbook;
import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import org.apache.commons.beanutils.converters.SqlDateConverter;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Lit et interprète un fichier XLS ou XLSX
 * Retourne l'ensemble des attributs suivants concernant le fichier soumis par l'utilisateur :
 * Nombre de feuilles, de lignes, de colonnes
 * Nom des titres / headers
 * Types des données de chaque colonne de chaque feuillet
 * Le type de données précis est retourné si et seulement si il a été trouvé dans toutes les cellules
 * de la colonne. Sinon, ExcelReader type les données en "text"
 */
public class ExcelReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelReader.class);

    private static final String EMPTY = "empty";
    private static final String BOOLEAN = "boolean";
    private static final String ERROR = "error";
    private static final String TIMESTAMP = "timestamp";
    private static final String DATE = "date";
    private static final String NUMERIC = "numeric";
    private static final String TEXT = "text";
    private static final String UNKNOWN = "unknown";
    private org.apache.poi.ss.usermodel.Workbook workbook;

    private List<InternalSheet> internalSheets;

    public ExcelReader(org.apache.poi.ss.usermodel.Workbook workbook) {
        this.workbook = workbook;
        this.internalSheets = new ArrayList<>();
        this.fetchData();
    }

    public ExcelReader(InputStream inputFile) {
        this(getWorkbookFromFile(inputFile));
    }

    private static org.apache.poi.ss.usermodel.Workbook getWorkbookFromFile(InputStream inputStream) {
        org.apache.poi.ss.usermodel.Workbook workbook;

        try {
            workbook = WorkbookFactory.create(inputStream);

        } catch (IOException | InvalidFormatException e) {
            LOGGER.error(e.getMessage());
            return null;
        }
        return workbook;
    }

    public static String getTypeFromCellTypeConstants(Cell cell, int typeConstant) {

        switch (typeConstant) {

            case Cell.CELL_TYPE_BLANK: {
                return EMPTY;
            }
            case Cell.CELL_TYPE_BOOLEAN: {
                return BOOLEAN;
            }
            case Cell.CELL_TYPE_ERROR: {
                return ERROR;
            }
            case Cell.CELL_TYPE_FORMULA: {
                return getTypeFromCellTypeConstants(cell, cell.getCachedFormulaResultType());
                //Renvoie les données calculées plutôt que la formule
            }
            case Cell.CELL_TYPE_NUMERIC: {
                //Cherche le type exact (numérique, date ou timestamp)
                if (cell.toString().matches(ColumnTypes.TIMESTAMP.regex)) {
                    return TIMESTAMP;
                } else if (DateUtil.isCellDateFormatted(cell)) {
                    setCorrectSqlDate(cell);
                    return DATE;
                } else {
                    return NUMERIC;
                }
            }
            case Cell.CELL_TYPE_STRING: {
                //En cas de conversion d'un fichier CSV, reconnaissance des types possibles
                if (cell.toString().matches(ColumnTypes.TIMESTAMP.regex)) {
                    return TIMESTAMP;
                } else if (cell.toString().matches(ColumnTypes.DATE.regex)) {
                    return DATE;
                } else if (cell.toString().matches(ColumnTypes.NUMERIC.regex)) {
                    return NUMERIC;
                }
                return TEXT;
            }
        }
        return UNKNOWN;
    }

    private static void setCorrectSqlDate(Cell cell) {
        SqlDateConverter converter = new SqlDateConverter();
        Object convertedDate = converter.convert(Date.class, cell.getDateCellValue());

        cell.setCellValue(convertedDate.toString());
    }

    private static String getComplementaryType(String inputType) {

        switch (inputType) {
            case "timestamp":
            case "text":
            case "date":
            case "boolean":
                return "dimension";
            case "numeric":
                return "measure";
        }
        return "undefined";
    }

    private void fetchData() {

        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {//pour chaque feuillet

            InternalSheet internalSheet = new InternalSheet();
            internalSheet.setBeginningLine(0);
            //par défaut : ligne header est à 0, paramétrable par l'utilisateur ensuite
            try {
                internalSheet.setRowCount(this.workbook.getSheetAt(i).getLastRowNum()); //nombre de lignes
                internalSheet.setColumnCount((int) this.workbook.getSheetAt(i).getRow(internalSheet.getRowCount()).getLastCellNum()); //nombre de colonnes
                internalSheet.setStandardTypes(getStandardTypes(i, internalSheet.getBeginningLine())); //types de données
                internalSheet.setHeadersText(getColumns(i, internalSheet.getBeginningLine())); //titres des colonnes
            } catch (Exception e) {//traitement des feuillets spéciaux (macro, graphiques) --> feuillets à ignorer
                internalSheet.setRowCount(0);
                internalSheet.setColumnCount(0);
                internalSheet.setHeadersText((new ArrayList<>()));
                LOGGER.error(e.getMessage());
            }
            this.internalSheets.add(internalSheet);
        }
        excelFileInfoLogger();
        //Optionnel : pour log quelques informations de base sur le classeur Excel
    }

    public org.apache.poi.ss.usermodel.Workbook getWorkbook() {

        return workbook;
    }

    private List<String> getStandardTypes(int sheetToExtract, int beginningLine) {
        //Retourne les types de chaque colonne d'une feuille Excel -- Avertit si les données sont différentes (log)
        List<String> primaryFoundTypes = new ArrayList<>();
        List<String> types = new ArrayList<>();
        boolean errorFound = false;

        int lastRowNum = workbook.getSheetAt(sheetToExtract).getLastRowNum();
        for (int i = beginningLine + 1; i <= lastRowNum; i++) {
            for (int j = 0; j < workbook.getSheetAt(sheetToExtract).getRow(lastRowNum).getLastCellNum(); j++) {
                try {
                    Cell cell = workbook.getSheetAt(sheetToExtract).getRow(i).getCell(j);
                    types.add(getTypeFromCellTypeConstants(cell, cell.getCellType()));
                } catch (Exception e) {
                    types.add("EMPTY");
                }
            }
            if (primaryFoundTypes.isEmpty()) {
                primaryFoundTypes = types;
            }
            if (!errorFound)
                try {
                    if (primaryFoundTypes != types) {
                        throw new DifferentTypesException("Incompatibilité des données : les colonnes sont composées de types de données différents.");
                    }
                } catch (DifferentTypesException dte) {
                    LOGGER.error("Expected :" + primaryFoundTypes.toString());
                    LOGGER.error("Observed :" + types.toString());
                    LOGGER.error("at line " + i);
                    LOGGER.error(dte.getMessage());
                    errorFound = true;
                }
        }
        return types;
    }

    private void excelFileInfoLogger() {

        LOGGER.info("Nombre de feuillets :" + workbook.getNumberOfSheets());
        LOGGER.info("Nombre de lignes (feuillet 1) : " + (internalSheets.get(0).getRowCount() + 1));
        LOGGER.info("Nombre de colonnes (feuillet 1) : " + internalSheets.get(0).getColumnCount());
    }

    public Workbook getInformation(int numberOfDisplayedRows) {
        //Retourne un objet complet Workbook décrivant n lignes d'un classeur Excel, à partir de la ligne donnée.
        Workbook workbook = new Workbook();

        for (int sheetNumber = 0; sheetNumber < this.workbook.getNumberOfSheets(); sheetNumber++) {
            //pour chaque feuillet
            int actualNumberOfLines = internalSheets.get(sheetNumber).getRowCount();
            if (numberOfDisplayedRows > actualNumberOfLines)
                numberOfDisplayedRows = actualNumberOfLines + 1;
            // si le nombre de lignes spécifié est trop important : il est ramené au nombre de lignes effectives
            WorkbookSheet currentSheet = new WorkbookSheet(this.workbook.getSheetName(sheetNumber), sheetNumber);
            currentSheet.setIndex(sheetNumber + 1);
            for (int col = 0; col < internalSheets.get(sheetNumber).getColumnCount(); col++) {
                LinkedHashMap<String, Object> colToAdd = new LinkedHashMap<>();
                //ajout de chaque colonne

                InternalSheet internalSheet = internalSheets.get(sheetNumber);
                if (internalSheet.getStandardTypes().size() >= col) {
                    colToAdd.put("name", getColumns(sheetNumber, internalSheet.getBeginningLine()).get(col));
                    String columnType = internalSheet.getStandardTypes().get(col);
                    String complementaryType = getComplementaryType(columnType);
                    colToAdd.put("type", complementaryType);
                    colToAdd.put("dataType", columnType);
                    if (complementaryType.equals("measure")) {
                        //prépare des tableaux vides pour les aggrégations / unités choisies oar l'utilisateur
                        colToAdd.put("aggregation", new ArrayList<String>());
                        colToAdd.put("unity", new ArrayList<String>());
                    }
                } else {
                    colToAdd.put("type", StringUtils.EMPTY);
                    colToAdd.put("dataType", StringUtils.EMPTY);
                }
                currentSheet.getColumns().add(colToAdd);
            }
            //ajout du tableau de cellules à partir de la ligne suivant le header
            int firstRow = this.internalSheets.get(sheetNumber).getBeginningLine() + 1;
            for (int row = firstRow; row < numberOfDisplayedRows; row++) {
                LinkedHashMap<String, String> cellToAdd = new LinkedHashMap<>();
                for (int col = 0; col < internalSheets.get(sheetNumber).getColumnCount(); col++) {
                    try {
                        cellToAdd.put(internalSheets.get(sheetNumber).
                                getHeadersText().get(col), getCellName(sheetNumber, row, col));
                    } catch (Exception e) {
                        cellToAdd.put(StringUtils.EMPTY, StringUtils.EMPTY);
                    }
                }
                currentSheet.getCells().add(cellToAdd);
            }
            workbook.addSheet(currentSheet);
        }
        return workbook;
    }

    private String getCellName(int sheetNumber, int row, int col) {
        String output;
        try {
            Cell cellToTest = workbook.getSheetAt(sheetNumber).getRow(row).getCell(col);
            if (cellToTest.getCellType() == Cell.CELL_TYPE_FORMULA) {
                output = String.valueOf(cellToTest.getCachedFormulaResultType());
            } else {
                output = cellToTest.toString();
            }
        } catch (Exception e) {
            return StringUtils.EMPTY;
        }
        return output;
    }

    private List<String> getColumns(int sheetNumber, Integer beginningLine) {
        //n° de ligne d'en-tête à passer en argument
        List<String> columns = new ArrayList<>();
        org.apache.poi.ss.usermodel.Sheet sheet = this.workbook.getSheetAt(sheetNumber);
        short lastColumnIndex = sheet.getRow(sheet.getLastRowNum()).getLastCellNum();
        for (int i = 0; i < lastColumnIndex; i++) {
            try {
                Row headerRow = sheet.getRow(beginningLine);
                columns.add(headerRow.getCell(i).toString());
            } catch (Exception e) {
                return new ArrayList<>();
            }
        }
        return columns;
    }
}
