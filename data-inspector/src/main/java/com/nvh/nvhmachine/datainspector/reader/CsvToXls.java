package com.nvh.nvhmachine.datainspector.reader;

import com.opencsv.CSVReader;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Convertit un fichier CSV en XLS
 */
public class CsvToXls {

    public static Workbook convert(InputStream csvStream) {
        //input : csv file ; output : xls file
        Workbook wb = new HSSFWorkbook();
        CreationHelper helper = wb.getCreationHelper();
        Sheet sheet = wb.createSheet(csvStream.toString());
        CSVReader reader = new CSVReader(new InputStreamReader(csvStream));
        String[] line;
        int r = 0;
        try {
            while ((line = reader.readNext()) != null) {
                Row row = sheet.createRow((short) r++);

                for (int i = 0; i < line.length; i++) {
                    row.createCell(i).setCellValue(helper.createRichTextString(line[i]));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return wb;
    }
}
