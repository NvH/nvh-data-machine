package com.nvh.nvhmachine.datainspector.reader;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe utilisée pour décrire les informatioons trouvées dans un feuillet :
 * Nombre de lignes, ligne de départ à utiliser (important car l'utilisateur a possibilité de la choisir à l'édition),
 * Nombre de colonnes, Types de données trouvés pour chaque colonne, intitulés de chaque colonne
 */
class InternalSheet {

    private int rowCount;
    private Integer beginningLine;
    private int columnCount;
    private List<String> standardTypes;
    private List<String> headersText;

    InternalSheet() {
        this.rowCount = 0;
        this.beginningLine = 0;
        this.columnCount = 0;
        this.standardTypes = new ArrayList<>();
        this.headersText = new ArrayList<>();
    }

    List<String> getHeadersText() {
        return headersText;
    }

    void setHeadersText(List<String> headersText) {
        this.headersText = headersText;
    }

    int getRowCount() {
        return rowCount;
    }

    void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    int getColumnCount() {
        return columnCount;
    }

    void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }

    Integer getBeginningLine() {
        return beginningLine;
    }

    void setBeginningLine(Integer beginningLine) {
        this.beginningLine = beginningLine;
    }

    List<String> getStandardTypes() {
        return standardTypes;
    }

    void setStandardTypes(List<String> standardTypes) {
        this.standardTypes = standardTypes;
    }
}
