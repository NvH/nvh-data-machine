package com.nvh.nvhmachine.datainspector.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Classe décrivant le classeur complet (ensemble de feuillets) d'un fichier importé.
 * Un 'Workbook' se compose d'un ensemble de 'WorkbookSheet' et d'une liste 'project' décrivant l'ensemble du fichier.
 * 'project' sera sérialisé et permettra le mappage pour l'enregistrement en base
 * (cf. /webapp/service/mapper/ProjectMapper.java)
 */
public class Workbook {

    public List<Map<String, String>> project;
    public List<WorkbookSheet> sheets;

    public Workbook() {
        this.sheets = new ArrayList<>();
    }

    public void addSheet(WorkbookSheet sheetToAdd) {
        this.sheets.add(sheetToAdd);
    }

    public List<WorkbookSheet> getSheets() {
        return sheets;
    }

    public List<Map<String, String>> getProject() {
        return project;
    }

    public void setProject(List<Map<String, String>> project) {
        this.project = project;
    }
}
