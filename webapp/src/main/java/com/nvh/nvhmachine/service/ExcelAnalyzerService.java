package com.nvh.nvhmachine.service;

import com.nvh.nvhmachine.datainspector.model.Workbook;
import com.nvh.nvhmachine.datainspector.reader.ExcelReader;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Service
public class ExcelAnalyzerService {
    public static InputStream multipartToFile(MultipartFile multipart) throws IOException {
        return multipart.getInputStream();
    }

    public Workbook getInformationFromFile(InputStream file, int numberOfRowsToDisplay) {

        ExcelReader xlsReader = new ExcelReader(file);
        return xlsReader.getInformation(numberOfRowsToDisplay);
    }
}
