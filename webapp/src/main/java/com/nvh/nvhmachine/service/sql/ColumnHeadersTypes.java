package com.nvh.nvhmachine.service.sql;

enum ColumnHeadersTypes {

    TEXT(Constants.TEXT),
    DATE(Constants.DATE),
    NUMERIC(Constants.NUMERIC),
    TIMESTAMP(Constants.TIMESTAMP);

    public final String sql;

    ColumnHeadersTypes(String sql) {
        this.sql = sql;
    }

    private static class Constants {
        static final String TEXT = "varchar (50)";
        static final String DATE = "DATE";
        static final String NUMERIC = "INTEGER";
        static final String TIMESTAMP = "TIMESTAMP";
    }
}
