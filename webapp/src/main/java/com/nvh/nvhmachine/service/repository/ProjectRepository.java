package com.nvh.nvhmachine.service.repository;

import com.nvh.nvhmachine.domain.Project;
import org.springframework.data.repository.CrudRepository;

public interface ProjectRepository extends CrudRepository<Project, String> {

}
