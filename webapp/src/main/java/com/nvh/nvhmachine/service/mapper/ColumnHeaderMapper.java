package com.nvh.nvhmachine.service.mapper;

import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import com.nvh.nvhmachine.domain.ColumnHeader;
import com.nvh.nvhmachine.domain.Project;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
class ColumnHeaderMapper {
    //Ce 'mappage' est utilisé dans ProjectMapper pour extraire tous les 'WorkbookSheet' depuis un projet complet
    List<ColumnHeader> mapFrom(WorkbookSheet workbookSheet) {

        List<ColumnHeader> columnHeaders = new ArrayList<>();

        Project initializationProject = new Project();
        initializationProject.setId(workbookSheet.formatName());

        for (Map<String, Object> column : workbookSheet.getColumns()) {
            ColumnHeader columnHeader = new ColumnHeader();
            columnHeader.setProject(initializationProject);
            columnHeader.setId((long) (columnHeaders.size() + 1));
            columnHeader.setName((String) column.get("name"));
            columnHeader.setType((String) column.get("type"));
            columnHeader.setDataType((String) column.get("dataType"));

            for (Object aggregation : Collections.singletonList(column.get("aggregation"))) {
                try {
                    columnHeader.setAggregation(aggregation.toString());
                } catch (Exception e) {
                    columnHeader.setAggregation("");
                }
            }
            for (Object unity : Collections.singletonList(column.get("unity"))) {
                try {
                    columnHeader.setUnity(unity.toString());
                } catch (Exception e) {
                    columnHeader.setUnity("");
                }
            }
            columnHeaders.add(columnHeader);
        }
        initializationProject.setColumnHeaders(columnHeaders);
        return columnHeaders;
    }

    //Ce 'mappage' est utilisé dans ProjectMapper pour obtenir un 'Project' depuis un ensemble de 'WorkbookSheet'
    WorkbookSheet mapFrom(Iterable<ColumnHeader> columnHeaders) {

        WorkbookSheet workbookSheet = new WorkbookSheet();

        List<Map<String, Object>> columns = new ArrayList<>();

        workbookSheet.setName(workbookSheet.getName());

        for (ColumnHeader columnHeader : columnHeaders) {
            LinkedHashMap<String, Object> colToAdd = new LinkedHashMap<>();
            colToAdd.put("name", columnHeader.getName());
            colToAdd.put("type", columnHeader.getType());
            colToAdd.put("dataType", columnHeader.getDataType());
            colToAdd.put("aggregation", columnHeader.getAggregation());
            colToAdd.put("unity", columnHeader.getUnity());
            columns.add(colToAdd);
        }
        workbookSheet.setColumns(columns);
        return workbookSheet;
    }
}
