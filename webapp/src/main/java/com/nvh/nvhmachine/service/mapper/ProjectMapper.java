package com.nvh.nvhmachine.service.mapper;

import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import com.nvh.nvhmachine.domain.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Permet de "mapper" depuis un 'WorkbookSheet' vers un 'Project' ou réciproquement
 */
@Service
public class ProjectMapper {

    @Autowired
    private
    ColumnHeaderMapper columnHeaderMapper;

    public Project mapFrom(WorkbookSheet workbookSheet) {

        Project project = new Project()
                .setId(workbookSheet.formatName());

        project.setColumnHeaders(columnHeaderMapper.mapFrom(workbookSheet));

        return project;
    }

    public WorkbookSheet mapFrom(Project project) {
        WorkbookSheet workbookSheet = columnHeaderMapper.mapFrom(project.getColumnHeaders());
        workbookSheet.setName(project.getId());
        return workbookSheet;
    }
}
