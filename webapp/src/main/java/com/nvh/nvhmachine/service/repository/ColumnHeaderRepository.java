package com.nvh.nvhmachine.service.repository;

import com.nvh.nvhmachine.domain.ColumnHeader;
import org.springframework.data.repository.CrudRepository;

public interface ColumnHeaderRepository extends CrudRepository<ColumnHeader, Long> {

}
