package com.nvh.nvhmachine.service;

import com.nvh.nvhmachine.datainspector.model.Workbook;
import com.nvh.nvhmachine.datainspector.reader.CsvToXls;
import com.nvh.nvhmachine.datainspector.reader.ExcelReader;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Service
public class CsvAnalyzerService {
    public Workbook getInformationFromFile(InputStream file, int numberOfRowsToDisplay) throws IOException {

        ExcelReader csvReader = new ExcelReader(CsvToXls.convert(file));
        return csvReader.getInformation(numberOfRowsToDisplay);
    }

    public InputStream multipartToFile(MultipartFile multipart) throws IOException {

        return multipart.getInputStream();
    }
}
