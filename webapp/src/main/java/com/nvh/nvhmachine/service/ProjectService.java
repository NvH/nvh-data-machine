package com.nvh.nvhmachine.service;

import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import com.nvh.nvhmachine.domain.ColumnHeader;
import com.nvh.nvhmachine.domain.Project;
import com.nvh.nvhmachine.service.mapper.ProjectMapper;
import com.nvh.nvhmachine.service.repository.ProjectRepository;
import com.nvh.nvhmachine.service.sql.StatementGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Service de lecture / écriture d'un 'Project' entier en base.
 */
@Service
public class ProjectService {
    private final static Logger LOGGER = LoggerFactory.getLogger(ProjectService.class);
    @Autowired
    private EmbeddedDatabase embeddedDatabase;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ProjectMapper projectMapper;

    public void saveWorkbookSheet(WorkbookSheet workbookSheet) {
        Project project = projectMapper.mapFrom(workbookSheet);
        StatementGenerator generator = new StatementGenerator();
        //effacement du schéma si celui-ci existe déjà
        String sqlRecordsDelete = generator.generateDeleteRecordStatement(project);
        try (PreparedStatement preparedStatement = embeddedDatabase.getConnection().prepareStatement(sqlRecordsDelete)) {
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        //effecement de la table de données si celle-ci existe déjà
        String sqlTableDelete = generator.generateDeleteTableStatement(project);
        try (PreparedStatement preparedStatement = embeddedDatabase.getConnection().prepareStatement(sqlTableDelete)) {
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        //création de la table de données
        String sqlTableCreation = generator.generateCreateStatement(project);
        try (PreparedStatement preparedStatement = embeddedDatabase.getConnection().prepareStatement(sqlTableCreation)) {
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        //insertion des données dans la table
        List<String> sqlTableInsertions = generator.generateInsertStatement(workbookSheet);
        for (String sql : sqlTableInsertions) {
            try (PreparedStatement preparedStatement = embeddedDatabase.getConnection().prepareStatement(sql)) {
                preparedStatement.execute();
            } catch (SQLException e) {
                LOGGER.error(e.getMessage());
            }
        }
        projectRepository.save(project);
    }

    public WorkbookSheet loadWorkbookSheet(String id) {
        //lecture de la structure depuis la base
        Project project = projectRepository.findOne(id);
        WorkbookSheet workbookSheet = projectMapper.mapFrom(project);

        //ajout des données aux tableaux "Cells"
        workbookSheet.setCells(retrieveCellsFromProject(project));
        return workbookSheet;
    }

    //Lecture des cellules d'un projet
    private List<Map<String, String>> retrieveCellsFromProject(Project project) {

        List<Map<String, String>> cells = new ArrayList<>();

        StatementGenerator statementGenerator = new StatementGenerator();
        String query = statementGenerator.generateSelectStatement(project);
        try (PreparedStatement ps = embeddedDatabase.getConnection().prepareStatement(query)) {

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                LinkedHashMap<String, String> cellToAdd = new LinkedHashMap<>();
                int index = 1;
                for (ColumnHeader columnHeader : project.getColumnHeaders()) {
                    String toPut = rs.getString(index++);
                    cellToAdd.put(columnHeader.getName(), toPut);
                }
                cells.add(cellToAdd);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        return cells;
    }
}
