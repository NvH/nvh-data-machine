package com.nvh.nvhmachine.service.sql;

import com.google.common.collect.Lists;
import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import com.nvh.nvhmachine.domain.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.join;

public class StatementGenerator {

    /**
    Génère le script SQL de crétion d'une nouvelle table à la volée en fonction du type d'objets
    trouvés dans chaque colonne
     */
    public String generateCreateStatement(Project project) {
        StringBuilder sb = new StringBuilder()
                .append("CREATE TABLE ")
                .append(project.getId())
                .append(" (");
        String joinedColumns = generateColumnHeadersStatement(project);
        sb.append(joinedColumns);
        sb.append(")");

        return sb.toString();
    }

    /**
    Génère le script SQL d'insertion des valeurs de chaque cellule du fichier CSV ou Excel
     */
    public List<String> generateInsertStatement(WorkbookSheet workbookSheet) {

        List<String> statements = new ArrayList<>();
        StringBuilder sb;
        List<Map<String, String>> cells = workbookSheet.getCells();
        for (Map cell : cells) {
            sb = new StringBuilder()
                    .append("INSERT INTO ")
                    .append(workbookSheet.formatName())
                    .append(" VALUES ('")
                    .append(join(cell.values(), "','"))
                    .append("');");

            statements.add(sb.toString());
        }
        return statements;
    }

    /**
        Génère le script SQL de sélection de l'ensemble des colonnes d'un projet donné
         */
    public String generateSelectStatement(Project project) {

        StringBuilder sb = new StringBuilder()
                .append("SELECT ");
        String joinedColumns = generateColumnHeadersNames(project);
        sb.append(joinedColumns)
                .append("FROM ")
                .append(project.getId());

        return sb.toString();
    }

    /**
        Génère le script SQL d'écrasement d'un projet en base si celui ci existe déjà
         */
    public String generateDeleteTableStatement(Project project) {
        return "drop table if exists " + project.getId() + ";";
    }

    /**
            Génère le script SQL d'écrasement des données d'un projet si celles-ci existent déjà
             */
    public String generateDeleteRecordStatement(Project project) {
        return "DELETE FROM COLUMN_HEADERS WHERE ID_PROJECT=" + project.getId() + ";";
    }

    /**
            Génère le script SQL de création de chaque champ COlumnHeader en fonction du type trouvé
            dans le fichier CSV ou Excel
             */
    private String generateColumnHeadersStatement(Project project) {
        List<String> columns = Lists.transform(project.getColumnHeaders(), columnHeader -> {
            assert columnHeader != null;
            return "\"" + columnHeader.getName() + "\" " + SQLType(columnHeader.getDataType());
        });
        return join(columns, ", ");
    }

    /**
            Génère le script SQL de nommage de chaque colonne du projet en utilisant le nom des colonnes
            du fichier source (CSV ou Excel)
             */
    private String generateColumnHeadersNames(Project project) {
        List<String> columns = Lists.transform(project.getColumnHeaders(), columnHeader -> {
            assert columnHeader != null;
            return "\"" + columnHeader.getName() + "\" ";
        });
        return join(columns, ", ");
    }

    /**
            Identifie le type SQL de chaque colonne
             */
    private String SQLType(String type) {
        switch (type) {
            case "timestamp":
                return ColumnHeadersTypes.TIMESTAMP.sql;
            case "date":
                return ColumnHeadersTypes.DATE.sql;
            case "numeric":
                return ColumnHeadersTypes.NUMERIC.sql;
        }
        return ColumnHeadersTypes.TEXT.sql;
    }

}
