package com.nvh.nvhmachine.configuration;

import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

class EmbeddedDatabase {

    private final static org.springframework.jdbc.datasource.embedded.EmbeddedDatabase database = new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.HSQL)
            .addScript("db/sql/schema.sql")
            .build();

    static org.springframework.jdbc.datasource.embedded.EmbeddedDatabase getInstance() {
        return database;
    }
}
