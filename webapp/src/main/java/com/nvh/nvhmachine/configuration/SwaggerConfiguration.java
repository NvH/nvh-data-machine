package com.nvh.nvhmachine.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
class SwaggerConfiguration {

    @Bean
    public Docket restfulApi() {
        ApiInfo info = new ApiInfo("nvh-Machine API", "Analyse de fichiers CSV, XLS et XLSX : typage des données", "0.1.0", "",
                "", "License", "");
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(info)
                .select()
                .build();
    }
}
