package com.nvh.nvhmachine.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity(name = "COLUMN_HEADERS")
public class ColumnHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    private String name;
    private String type;
    private String dataType;
    private String aggregation;
    private String unity;

    @ManyToOne
    @JoinColumn(name = "ID_PROJECT")
    private Project project;

    public ColumnHeader() {

    }

    public ColumnHeader(Long id, String name, String type, String dataType, String aggregation, String unity) {
        this.name = name;
        this.type = type;
        this.dataType = dataType;
        this.aggregation = aggregation;
        this.unity = unity;
    }


    public ColumnHeader setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ColumnHeader setName(String name) {
        this.name = name;
        return this;
    }

    public String getType() {
        return type;
    }

    public ColumnHeader setType(String type) {
        this.type = type;
        return this;
    }

    public String getDataType() {
        return dataType;
    }

    public ColumnHeader setDataType(String dataType) {
        this.dataType = dataType;
        return this;
    }

    public String getAggregation() {
        return aggregation;
    }

    public ColumnHeader setAggregation(String aggregation) {
        this.aggregation = aggregation;
        return this;
    }

    public String getUnity() {
        return unity;
    }

    public ColumnHeader setUnity(String unity) {
        this.unity = unity;
        return this;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

}
