package com.nvh.nvhmachine.domain;

import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;

@Entity(name = "NVH_MACHINE_PROJECT")
public class Project extends WorkbookSheet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_PROJECT")
    private String id;

    @Column(name = "CREATION_DATE")
    private Date creationDate;

    @OneToMany(mappedBy = "project", cascade = ALL, fetch = EAGER/*, fetch = LAZY*/)
    private List<ColumnHeader> columnHeaders;

    public Project() {
        this.columnHeaders = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public Project setId(String idProject) {
        this.id = idProject;
        return this;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Project setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public Project addColumnHeader(ColumnHeader columnHeader) {
        columnHeader.setProject(this);
        this.getColumnHeaders().add(columnHeader);
        return this;
    }

    public List<ColumnHeader> getColumnHeaders() {
        return columnHeaders;
    }

    public void setColumnHeaders(List<ColumnHeader> columnHeaders) {
        this.columnHeaders = columnHeaders;
    }

}
