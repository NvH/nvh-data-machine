package com.nvh.nvhmachine.controller;

import com.nvh.nvhmachine.datainspector.model.Workbook;
import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import com.nvh.nvhmachine.service.ExcelAnalyzerService;
import com.nvh.nvhmachine.service.ProjectService;
import com.nvh.nvhmachine.service.repository.ColumnHeaderRepository;
import com.nvh.nvhmachine.service.repository.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static com.nvh.nvhmachine.service.ExcelAnalyzerService.multipartToFile;

@RestController
@RequestMapping(value = "/reader")
public class ExcelReaderController {

    private final static Logger LOGGER = LoggerFactory.getLogger(ExcelReaderController.class);

    @Autowired
    private ExcelAnalyzerService excelAnalyzerService;

    @Autowired
    private ColumnHeaderRepository columnHeaderRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/xls", method = RequestMethod.POST)
    public ResponseEntity<Workbook> importFile(@RequestParam(value = "xlsfile") MultipartFile mpFile) {

        int rowsToDisplay = 50; //paramètrable ensuite

        try {
            Workbook returnedResults = excelAnalyzerService.getInformationFromFile(multipartToFile(mpFile), rowsToDisplay);

            //sauvegarde des données
            for (WorkbookSheet sheet : returnedResults.getSheets()) {
                projectService.saveWorkbookSheet(sheet);
            }
            return new ResponseEntity<>(returnedResults, HttpStatus.OK);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/displayXls", method = RequestMethod.GET)
    public WorkbookSheet display(@RequestParam String idProject) {
        return projectService.loadWorkbookSheet(idProject);
    }
}
