package com.nvh.nvhmachine.controller;

import com.nvh.nvhmachine.datainspector.model.Workbook;
import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import com.nvh.nvhmachine.service.CsvAnalyzerService;
import com.nvh.nvhmachine.service.ExcelAnalyzerService;
import com.nvh.nvhmachine.service.ProjectService;
import com.nvh.nvhmachine.service.repository.ColumnHeaderRepository;
import com.nvh.nvhmachine.service.repository.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@RestController
@RequestMapping(value = "/reader")
public class CsvReaderController {

    private final Logger LOGGER = LoggerFactory.getLogger(CsvReaderController.class);

    @Autowired
    private CsvAnalyzerService csvAnalyzerService;

    @Autowired
    private ExcelAnalyzerService excelAnalyzerService;

    @Autowired
    private ColumnHeaderRepository columnHeaderRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/csv", method = RequestMethod.POST)
    public ResponseEntity<Workbook> importFile(@RequestParam(value = "csvfile") MultipartFile mpFile) {

        int rowsToDisplay = 50; //paramètrable ensuite

        try {
            InputStream inputStream = csvAnalyzerService.multipartToFile(mpFile);
            Workbook returnedResults = csvAnalyzerService.getInformationFromFile(inputStream, rowsToDisplay);
            //sauvegarde des données
            projectService.saveWorkbookSheet(returnedResults.sheets.get(0));

            return new ResponseEntity<>(returnedResults, HttpStatus.OK);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/displayCsv", method = RequestMethod.GET)
    public WorkbookSheet display(@RequestParam String idProject) {
        return projectService.loadWorkbookSheet(idProject);
    }
}
