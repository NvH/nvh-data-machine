package com.nvh.nvhmachine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication
public class NvhMachineApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(NvhMachineApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(NvhMachineApplication.class);
    }
}
