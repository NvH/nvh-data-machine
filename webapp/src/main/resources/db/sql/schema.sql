drop schema NVH_MACHINE if exists;
create schema NVH_MACHINE ;

create table if not exists NVH_MACHINE_PROJECT (
-- CLÉ sous le format :etab_name+nom_WB+nom_feuille
    ID_PROJECT varchar(100) not null,
    CREATION_DATE DATE
    );

create table if not exists COLUMN_HEADERS (
-- clé multiple : NAME_TYPE_AGGREGATION
    ID INTEGER,
    ID_PROJECT varchar(100) not null,
    NAME varchar(100) not null, -- titre colonne , peut se répeter (en fonction de l'aggregation)
    TYPE varchar(50) not null,
    DATATYPE varchar(50) not null,
    AGGREGATION varchar(50) DEFAULT '',
    UNITY varchar(50) DEFAULT ''
    );
