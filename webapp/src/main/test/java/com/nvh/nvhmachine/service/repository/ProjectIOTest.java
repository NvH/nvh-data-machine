package com.nvh.nvhmachine.service.repository;

import com.nvh.nvhmachine.datainspector.model.Workbook;
import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import com.nvh.nvhmachine.datainspector.reader.ExcelReader;
import com.nvh.nvhmachine.service.ProjectService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

public class ProjectIOTest extends GenericTest {

    @Autowired
    private ProjectService projectService;
    private ExcelReader test1, test2;

    @Before
    public void setupTest() throws IOException {

        URL resource1 = ProjectIOTest.class.getResource("/jsontest.xlsx");
        URL resource2 = ProjectIOTest.class.getResource("/Q4.xlsx");
        test1 = new ExcelReader(new FileInputStream(resource1.getFile()));
        test2 = new ExcelReader(new FileInputStream(resource2.getFile()));
    }

    @Test
    public void shouldSaveAndRetrieveWorkbookSheet() {

        Workbook workbook1 = test1.getInformation(50);
        //doit écrire en base
        projectService.saveWorkbookSheet(workbook1.sheets.get(0));

        //doit lire en base
        WorkbookSheet testWbs1 = projectService.loadWorkbookSheet(workbook1.sheets.get(0).formatName());

        assertThat(testWbs1.getCells().size()).isEqualTo(3);
    }

    @Test
    //Permet de tester la bonne écriture/lecture de WOrkbook différents identifiés par leur ID
    public void shouldSaveAndRetrieveMultipleWorkbookSheets() {

        Workbook workbook1 = test1.getInformation(50);
        Workbook workbook2 = test2.getInformation(50);

        //doit écraser PUIS écrire en base (workbook1) ; écrire en base (workbook2)
        projectService.saveWorkbookSheet(workbook1.sheets.get(0));
        projectService.saveWorkbookSheet(workbook2.sheets.get(0));

        //doit lire en base le premier et le second Workbook
        WorkbookSheet testWbs1 = projectService.loadWorkbookSheet(workbook1.sheets.get(0).formatName());
        WorkbookSheet testWbs2 = projectService.loadWorkbookSheet(workbook2.sheets.get(0).formatName());

        assertThat(testWbs1.getCells().size()).isEqualTo(3);
        assertThat(testWbs2.getCells().size()).isGreaterThan(3);

    }
}
