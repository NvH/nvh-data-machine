package com.nvh.nvhmachine.service.repository;

import com.nvh.nvhmachine.domain.Project;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;

public class ProjectRepositoryTest extends GenericTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    public void findCheckedReturnedItems() throws SQLException {

        Project project = projectRepository.findOne("etabTest_Feuille1");
        assertThat(project).isNotNull();

    }
}
