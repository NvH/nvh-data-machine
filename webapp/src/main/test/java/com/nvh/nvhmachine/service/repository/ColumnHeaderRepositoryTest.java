package com.nvh.nvhmachine.service.repository;

import com.nvh.nvhmachine.domain.ColumnHeader;
import com.nvh.nvhmachine.domain.Project;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class ColumnHeaderRepositoryTest extends GenericTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ColumnHeaderRepository columnHeaderRepository;

    @Test
    public void shouldFetchColumnHeaderFromProject() {
        Project project = new Project().setId("project-id-1");
        ColumnHeader columnHeader = new ColumnHeader()
                .setId(42L)
                .setName("column-header-name-1")
                .setType("dimension")
                .setDataType("text");
        project.addColumnHeader(columnHeader);

        projectRepository.save(project);
        assertThat(columnHeaderRepository.findOne(42L)).isNotNull();
    }

//    @Test
//    public void shouldRetrieveColumnHeaderFromProject() {
//
//        WorkbookSheet workbookSheet = projectRepository.findOne("project-id-1");
//        assertThat(workbookSheet.formatName().equals("project-id-1"));
////        assertThat(workbookSheet.getColumns().get(0).get("Name").equals("column-header-name-1"));
//    }
}
