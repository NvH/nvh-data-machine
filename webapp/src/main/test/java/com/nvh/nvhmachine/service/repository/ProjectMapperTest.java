package com.nvh.nvhmachine.service.repository;

import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import com.nvh.nvhmachine.domain.Project;
import com.nvh.nvhmachine.service.mapper.ProjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class ProjectMapperTest extends GenericTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectMapper projectMapper;

    @Test
    public void shouldMapProject() {
        Project project = projectRepository.findAll().iterator().next();
        WorkbookSheet workbookSheet = projectMapper.mapFrom(project);
        assertThat(workbookSheet).isNotNull();
    }
}
