package com.nvh.nvhmachine.service.repository;

import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

class EmbeddedDatabase {

    private static org.springframework.jdbc.datasource.embedded.EmbeddedDatabase database = new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.HSQL)
            .addScript("db/sql/schema.sql")
            .addScript("db/sql/insert-data.sql")
            .build();

    static org.springframework.jdbc.datasource.embedded.EmbeddedDatabase getInstance() {
        return database;
    }
}
