package com.nvh.nvhmachine.service.repository;

import com.nvh.nvhmachine.datainspector.model.Workbook;
import com.nvh.nvhmachine.datainspector.reader.ExcelReader;
import com.nvh.nvhmachine.domain.Project;
import com.nvh.nvhmachine.service.ProjectService;
import com.nvh.nvhmachine.service.mapper.ProjectMapper;
import com.nvh.nvhmachine.service.sql.StatementGenerator;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

public class SelectStatementGeneratorTest extends GenericTest {

    private static ExcelReader test;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ProjectMapper projectMapper;

    private StatementGenerator generator = new StatementGenerator();

    @Before
    public void setupTest() {

        URL resource = SelectStatementGeneratorTest.class.getResource("/jsontest.xlsx");
        try {
            test = new ExcelReader(new FileInputStream(resource.getFile()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldGenerateSelectionFromProject() {
        Workbook workbook = test.getInformation(50);
        Project project = projectMapper.mapFrom(workbook.sheets.get(0));
        String createSelect = generator.generateSelectStatement(project);

        assertThat(createSelect).isEqualTo
                ("SELECT \"Name \" , \"Age\" , \"Givename\" , \"Salaire\" , \"Birthdate\" FROM REF_Feuille_1");
    }
}
