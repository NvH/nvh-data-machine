package com.nvh.nvhmachine.service.repository;

import com.nvh.nvhmachine.domain.Project;
import com.nvh.nvhmachine.service.sql.StatementGenerator;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateStatementGeneratorTest extends GenericTest {

    @Autowired
    ProjectRepository projectRepository;

    @Test
    public void shouldGenerateCorrectStatementFromDatabase() {

        StatementGenerator generator = new StatementGenerator();
        Project project = projectRepository.findOne("etabTest_Feuille1");

        String createStatement = generator.generateCreateStatement(project);
        assertThat(createStatement).isEqualTo("CREATE TABLE etabTest_Feuille1 (\"Name\" varchar (50), " +
                "\"Age\" INTEGER, \"Salaire\" INTEGER)");
    }
}
