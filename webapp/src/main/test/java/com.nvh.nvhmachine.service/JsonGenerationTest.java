package com.nvh.nvhmachine.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nvh.nvhmachine.datainspector.model.Workbook;
import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;

public class JsonGenerationTest {

    @Test
    public void testJsonSerialization() throws JsonProcessingException {
        Workbook workbook = new Workbook();
        workbook.sheets.add(new WorkbookSheet("Test_Sheet1", 0));
        workbook.sheets.get(0).setName("Feuille1");
        workbook.sheets.add(new WorkbookSheet("Test_Sheet2", 0));
        workbook.sheets.get(1).setName("Feuille2");

        workbook.sheets.get(0).setCells(new ArrayList<>());
        workbook.sheets.get(0).setColumns(new ArrayList<>());
        HashMap<String, Object> colToAdd = new HashMap<>();
        colToAdd.put("name", "nom-col1");
        workbook.sheets.get(0).getColumns().add(colToAdd);
        workbook.sheets.get(1).getColumns().add(colToAdd);
        HashMap<String, String> cellToAdd = new HashMap<>();
        cellToAdd.put("Test_cell1", "Value");
        workbook.sheets.get(0).getCells().add(cellToAdd);
        workbook.sheets.get(1).getCells().add(cellToAdd);

        String serializedWorkbook = new ObjectMapper().writeValueAsString(workbook);

        assertThat(serializedWorkbook.equals("{\"sheets\":[{\"name\":\"Feuille1\",\"columns\":[{\"name\":\"nom-col1\"}],\"cells\":" +
                "[{\"Test_cell1\":\"Value\"}]},{\"name\":\"Feuille2\",\"columns\":" +
                "[{\"name\":\"nom-col1\"}],\"cells\":[{\"Test_cell1\":\"Value\"}]}]}"));
    }
}
