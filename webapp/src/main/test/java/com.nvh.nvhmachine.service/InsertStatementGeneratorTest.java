package com.nvh.nvhmachine.service;

import com.nvh.nvhmachine.datainspector.model.Workbook;
import com.nvh.nvhmachine.datainspector.model.WorkbookSheet;
import com.nvh.nvhmachine.datainspector.reader.ExcelReader;
import com.nvh.nvhmachine.service.sql.StatementGenerator;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class InsertStatementGeneratorTest {

    private ExcelReader test;

    private StatementGenerator generator = new StatementGenerator();

    @Before
    public void setupTest() throws IOException {

        URL resource = InsertStatementGeneratorTest.class.getResource("/jsontest.xlsx");
        test = new ExcelReader(new FileInputStream(resource.getFile()));
    }

    @Test
    public void shouldGenerateInsertionFromManualWorkbook() {

        Workbook workbook = new Workbook();
        workbook.sheets.add(new WorkbookSheet("Test_Sheet1", 0));
        workbook.sheets.get(0).setName("Feuille1");
        workbook.sheets.add(new WorkbookSheet("Test_Sheet2", 0));
        workbook.sheets.get(1).setName("Feuille2");

        workbook.sheets.get(0).setCells(new ArrayList<>());
        workbook.sheets.get(0).setColumns(new ArrayList<>());
        HashMap<String, Object> colToAdd = new HashMap<>();
        colToAdd.put("name", "nom-col1");
        workbook.sheets.get(0).getColumns().add(colToAdd);
        workbook.sheets.get(1).getColumns().add(colToAdd);
        HashMap<String, String> cellToAdd = new HashMap<>();
        cellToAdd.put("Test_cell1", "Value");
        workbook.sheets.get(0).getCells().add(cellToAdd);
        workbook.sheets.get(1).getCells().add(cellToAdd);

        List<String> createInsert = generator.generateInsertStatement(workbook.sheets.get(0));
        assertThat(createInsert.get(0)).isEqualTo("INSERT INTO REF_Feuille1 VALUES ('Value');");
    }

    @Test
    public void shouldGenerateInsertionFromLoadedWorkbook() {
        Workbook workbook = test.getInformation(50);
        List<String> createInsert = generator.generateInsertStatement(workbook.sheets.get(0));


        assertThat(createInsert.get(0)).contains(
                "INSERT INTO REF_Feuille_1 VALUES ('Mark','20.0','@markdo','1900.0'");
    }
}
