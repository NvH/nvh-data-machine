import com.nvh.nvhmachine.domain.ColumnHeader;
import com.nvh.nvhmachine.domain.Project;
import com.nvh.nvhmachine.service.sql.StatementGenerator;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class StatementGeneratorTest {

    private static ColumnHeader newColumnHeader(String dataType, String name) {
        return new ColumnHeader().setDataType(dataType).setName(name);
    }

    private static Project newProject(String projectId, ColumnHeader... columnHeaders) {
        Project project = new Project().setId(projectId);
        project.getColumnHeaders().addAll(Arrays.asList(columnHeaders));
        return project;
    }

    @Test
    public void shouldGenerateStatementFromProjectWithOneColumn() {

        Project project = newProject("ETAB_TEST", newColumnHeader("text", "FamilyName"));

        StatementGenerator generator = new StatementGenerator();
        String createStatement = generator.generateCreateStatement(project);

        assertThat(createStatement).isEqualTo("CREATE TABLE ETAB_TEST (\"FamilyName\" varchar (50))");
    }

    @Test
    public void shouldGenerateStatementFromProjectWithMultipleColumns() {
        Project project = newProject("ETAB_TEST",
                newColumnHeader("text", "FamilyName"),
                newColumnHeader("text", "FirstName"),
                newColumnHeader("numeric", "Age"));

        StatementGenerator generator = new StatementGenerator();
        String createStatement = generator.generateCreateStatement(project);

        assertThat(createStatement).isEqualTo("CREATE TABLE ETAB_TEST (\"FamilyName\" varchar (50), \"FirstName\" varchar (50), \"Age\" INTEGER)");
    }
}
